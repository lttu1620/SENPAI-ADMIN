<?php

class cacheObj {

    public $key = null;
    public $seconds = 86400; // 24*60*60

    public function __construct($key = null, $seconds = null) {
        $this->key = $key;
        $this->seconds = $seconds;
    }

}

Configure::write('questions_all', new cacheObj('questions_all', 60 * 60));
Configure::write('categories_all', new cacheObj('categories_all', 60 * 60));
Configure::write('global_settings', new cacheObj('global_settings', 60 * 60));
Configure::write('segments_all', new cacheObj('segments_all', 60 * 60));
Configure::write('enrollments_all', new cacheObj('enrollments_all', 60 * 60));
Configure::write('usergroups_all', new cacheObj('usergroups_all', 60 * 60));
Configure::write('report_general', new cacheObj('report_general', 60 * 60));
