<?php
Configure::write('API.Timeout', 30);

Configure::write('API.url_answers_list', 'answers/list');
Configure::write('API.url_answers_detail', 'answers/detail');
Configure::write('API.url_answers_disable', 'answers/disable');
Configure::write('API.url_answers_add', 'answers/add');
Configure::write('API.url_answers_update', 'answers/update');

Configure::write('API.url_admins_list', 'admins/list');
Configure::write('API.url_admins_detail', 'admins/detail');
Configure::write('API.url_admins_disable', 'admins/disable');
Configure::write('API.url_admins_addupdate', 'admins/addupdate');
Configure::write('API.url_admins_login', 'admins/login');
Configure::write('API.url_admins_password', 'admins/updatepassword');

Configure::write('API.url_categories_all', 'categories/all');
Configure::write('API.url_categories_list', 'categories/list');
Configure::write('API.url_categories_detail', 'categories/detail');
Configure::write('API.url_categories_addupdate', 'categories/addupdate');

Configure::write('API.url_questions_detail', 'questions/detail');
Configure::write('API.url_questions_add', 'questions/add');
Configure::write('API.url_questions_update', 'questions/update');
Configure::write('API.url_questions_all', 'questions/all');
Configure::write('API.url_questions_list', 'questions/list');

Configure::write('API.url_informations_detail', 'informations/detail');
Configure::write('API.url_informations_addupdate', 'informations/addupdate');
Configure::write('API.url_informations_all', 'informations/all');
Configure::write('API.url_informations_list', 'informations/list');

Configure::write('API.url_questionfavorites_list', 'questionfavorites/list');
Configure::write('API.url_questionnices_list', 'questionnices/list');

Configure::write('API.url_applications_detail', 'applications/detail');
Configure::write('API.url_applications_add', 'applications/add');
Configure::write('API.url_applications_update', 'applications/update');
Configure::write('API.url_applications_all', 'applications/all');
Configure::write('API.url_applications_list', 'applications/list');


Configure::write('API.url_upload_image', 'upload/image.json');


Configure::write('API.url_settings_all', 'settings/all');
Configure::write('API.url_settings_list', 'settings/list');
Configure::write('API.url_settings_detail', 'settings/detail');
Configure::write('API.url_settings_addupdate', 'settings/addupdate');
Configure::write('API.url_settings_multiupdate', 'settings/multiupdate');

Configure::write('API.url_usersettings_all', 'usersettings/all');
Configure::write('API.url_usersettings_disable', 'usersettings/disable');
Configure::write('API.url_usersettings_addupdate', 'usersettings/addupdate');
Configure::write('API.url_usersettings_multiupdate', 'usersettings/multiupdate');


Configure::write('API.url_adminsettings_all', 'adminsettings/all');
Configure::write('API.url_adminsettings_disable', 'adminsettings/disable');
Configure::write('API.url_adminsettings_addupdate', 'adminsettings/addupdate');
Configure::write('API.url_adminsettings_multiupdate', 'adminsettings/multiupdate');

Configure::write('API.url_usergroups_all', 'usergroups/all');
Configure::write('API.url_usergroups_list', 'usergroups/list');
Configure::write('API.url_usergroups_detail', 'usergroups/detail');
Configure::write('API.url_usergroups_addupdate', 'usergroups/addupdate');

Configure::write('API.url_segments_list', 'segments/list');
Configure::write('API.url_segments_detail', 'segments/detail');
Configure::write('API.url_segments_addupdate', 'segments/addupdate');
Configure::write('API.url_segments_all', 'segments/all');

Configure::write('API.url_enrollments_all', 'enrollments/all');
Configure::write('API.url_enrollments_list', 'enrollments/list');
Configure::write('API.url_enrollments_detail', 'enrollments/detail');
Configure::write('API.url_enrollments_addupdate', 'enrollments/addupdate');


Configure::write('API.url_users_list', 'users/list');
Configure::write('API.url_users_detail', 'users/detail');
Configure::write('API.url_users_addupdate', 'users/addupdate');

Configure::write('API.url_applications_list', 'applications/list');
Configure::write('API.url_applications_detail', 'applications/detail');
Configure::write('API.url_applications_addupdate', 'applications/addupdate');

Configure::write('API.url_system_deletecache', 'system/deletecache');
Configure::write('API.url_reports_general', 'reports/general');