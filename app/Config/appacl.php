<?php
/*
 * Ambassador (admins.admin_type = 0)
 */
Configure::write('ACL.ambassador.allow', array(
	'admins/password' => '\/admins\/password',  
	'answers' => '\/answers',
	'questions' => '\/questions',  
	'users' => '\/users',
	'settings' => '\/settings',
	'loginlogs' => '\/loginlogs', 
	));    
Configure::write('ACL.ambassador.deny', array(
	'admins' => '\/admins',
	'settings' => '\/settings',
	'adminsettings' => '\/adminsettings',    
	'usersettings' => '\/usersettings',    
	));    