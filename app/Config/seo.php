<?php

Configure::write('seo', array(
    'default' => array(
        'title' => __('Senpai admin'),
        'description' => '',
        'keywords' => '',
    ),
    'admins' => array(
        'index' => array(
            'title' => __('Admin list'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'profile' => array(
            'title' => __('Change profile'),
        ),
        'update' => array(
            'title' => __('Add/update Admin'),
        )
    ),
    'adminsettings' => array(
        'index' => array(
            'title' => __('Admin settings'),
        )
    ),
    'Answers' => array(
        'index' => array(
            'title' => __('Answers list'),
        ),
        'update' => array(
            'title' => __('Add/update answer'),
        )
    ),
    'categories' => array(
        'follower' => array(
            'title' => __('Category follower list'),
        ),
        'index' => array(
            'title' => __('Category list'),
        ),
        'update' => array(
            'title' => __('Add/update Category'),
        )
    ),
    'loginlogs' => array(
        'index' => array(
            'title' => __('Login log list'),
        )
    ),
    'pages' => array(
        'index' => array(
            'title' => __('Dashboard'),
        ),
        'display' => array(
            'title' => __('Display news feed'),
        ),
        'login' => array(
            'title' => __('Sign In'),
        ),
        'fblogin' => array(
            'title' => __('Login facebook'),
        ),
        'register' => array(
            'title' => __('Register New Membership'),
        ),
        'registerprofile' => array(
            'title' => __('Update profile'),
        ),
        'registercompany' => array(
            'title' => __('Register company'),
        ),
        'registeractive' => array(
            'title' => __('Register active'),
        ),
        'registerapprove' => array(
            'title' => __('Register approve'),
        ),
        'logout' => array(
            'title' => __('Logout'),
        ),
        'pending' => array(
            'title' => __('Notice'),
        ),
        'forgetpassword' => array(
            'title' => __('Forget password'),
        ),
        'newpassword' => array(
            'title' => __('New password'),
        ),
        'lp' => array(
            'title' => 'senpai',
        ),
        'contact' => array(
            'title' => __('Contact form'),
        ),
        'privacypolicy' => array(
            'title' => 'プライバシーポリシー - senpai',
        )
    ),
    
    'settings' => array(
        'index' => array(
            'title' => __('Settings list'),
        ),
        'update' => array(
            'title' => __('Add/update settings'),
        )
    ),
   
    'startlogs' => array(
        'index' => array(
            'title' => __('Start log list'),
        )
    ),   
    'users' => array(
        'index' => array(
            'title' => __('User list'),
        ),
        'update' => array(
            'title' => __('Register user'),
        ),
        'profile' => array(
            'title' => __('Update user profile'),
        ),
        'profileinformation' => array(
            'title' => __('Update user information'),
        ),
        'recruiterinformation' => array(
            'title' => __('Update recruiter profile'),
        ),
        'facebookinformation' => array(
            'title' => __('Update user facebook information'),
        ),
        'password' => array(
            'title' => __('Change password'),
        ),
        'newsfeedfavorites' => array(
            'title' => __('News feed favorites list'),
        )
    ),
    'usersettings' => array(
        'index' => array(
            'title' => __('User settings'),
        )
    )
));

