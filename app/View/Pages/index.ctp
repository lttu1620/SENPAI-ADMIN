<!-- Main content -->       
    <h2>
        <i class="fa fa-dashboard"></i>&nbsp;
        <?php echo __('Dashboard'); ?> <small>[<?php echo date("Y年m月d日 H時i分")."&nbsp;".__("Update");?>]
        </small>
    </h2>
<!--     <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i><a href="#"><i class="fa fa-user"></i> 会員</a>
        </li>
        <li class="active">総会員数</li>
    </ol> -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
              <h3><?php echo empty($data['question_pending_count']) ? 0 : $data['question_pending_count'];?><sup style="font-size: 20px">件</sup></h3>
            <p>新着質問（未選別）</p>
          </div>
          <div class="icon">
            <i class="fa fa-envelope"></i>
          </div>
          <a href="<?php echo($this->Html->url('/questions?&status=0&disable=0'));?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?php echo empty($data['answer_pending_count']) ? 0 : $data['answer_pending_count'];?><sup style="font-size: 20px">件</sup></h3>
            <p>新着回答（未選別）</p>
          </div>
          <div class="icon">
            <i class="fa fa-comment"></i>
          </div>
          <a href="<?php echo($this->Html->url('/answers?&status=0&disable=0'));?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
<!-- 
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>999<sup style="font-size: 20px">件</sup></h3>
            <p>新着質問違反報告</p>
          </div>
          <div class="icon">
            <i class="fa fa-exclamation-triangle"></i>
          </div>
          <a href="<?php echo($this->Html->url('/questions?report=1&disable=0'));?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>999<sup style="font-size: 20px">件</sup></h3>
            <p>新着回答違反報告</p>
          </div>
          <div class="icon">
            <i class="fa fa-exclamation-triangle"></i>
          </div>
          <a href="<?php echo($this->Html->url('/answers?report=1&disable=0'));?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
 -->
