<?php echo $this->Html->image('logo.png'); ?>
<br />
<div class="header"><?php echo __('Register New Membership')?></div>
<div class="body bg-gray">
    <?php 
        echo $this->SimpleForm->render($updateForm); 
    ?>
</div>
<div class="footer">
    <a href="<?php echo $this->html->url('/login'); ?>"><i class="fa fa-sign-in"></i>&nbsp;<?php echo __('already registered.');?></a><br />
    <a href="<?php echo $this->html->url('/forgetpassword'); ?>" class="text-center"><i class="fa fa-exclamation-circle"></i>&nbsp;<?php echo __('if you Forget password.')?></a>
</div>