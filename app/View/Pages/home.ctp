<!-- Main content -->       
    <h2>
        総会員数 <small>[2014-12-29 07:11]
        </small>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="#"><i class="fa fa-user"></i> 会員</a>
        </li>
        <li class="active">総会員数</li>
    </ol>

    <div class="col-lg-6 col-xs-12">
        <!-- users number -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    1657                人
                </h3>
                <p>現在のアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
            <div class="small-box-footer">アプリ合計数</div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- ios users number -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>
                    1224                人
                </h3>
                <p>現在のiPhoneアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-apple"></i>
            </div>
            <div class="small-box-footer"><?php echo __('iPhone')?></div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- android users number -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    433             人
                </h3>
                <p>現在のAndroidアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-android"></i>
            </div>
            <div class="small-box-footer"><?php echo __('Android')?></div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- men users number -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>
                    768             人
                </h3>
                <p>現在のアプリ男性会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-male"></i>
            </div>
            <div class="small-box-footer">男性ユーザ</div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- female users number -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    889             人
                </h3>
                <p>現在のアプリ女性会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-female"></i>
            </div>
            <div class="small-box-footer">女性ユーザ</div>
        </div>
    </div>


    <div class="col-lg-3 col-xs-6">
        <!-- total clap number -->
        <div class="small-box bg-teal">
            <div class="inner">
                <h3>
                    497             回
                </h3>
                <p>現在の総クラップ数</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
            <div class="small-box-footer">Claps</div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <!-- total capture number -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>
                    47             回
                </h3>
                <p>現在の総キャプチャー数</p>
            </div>
            <div class="icon">
                <i class="fa fa-paperclip"></i>
            </div>
            <div class="small-box-footer"><?php echo __('Captures')?></div>
        </div>
    </div>

