
<?php echo $this->Html->image('logo.png',array("width"=>"390px")); ?>
<!-- 2015-03-13:    New sesion -->
<div class="login-logo">
</div><!-- /.login-logo -->
<div class="login-box-body">
    <p class="login-box-msg"><?php echo __('Admin page Sign In') ?></p>

    <?php echo $this->Form->Create('Page', array('type' => 'post', 'action' => 'login')); ?>

    <div class="form-group has-feedback">
        <?php
            echo $this->Form->input('Page.login', array(
                'id' => 'login',
                'class' => 'form-control',
                'placeholder' => __('Login ID'),
                'div' => false,
                'label' => false,
                'type' => 'type',
                'value' => empty($adm_cookie['login']) ? '' : $adm_cookie['login'],
            ));
       
        ?>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <?php
        echo $this->Form->input('Page.password', array(
            'id' => 'password',
            'class' => 'form-control',
            'placeholder' => __('Password'),
            'div' => false,
            'label' => false,
            'type' => 'password',
            'value' => (empty($adm_cookie['admin_password']) ? '' : $adm_cookie['admin_password'])
        ));
        
        ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="checkbox icheck">
                <label>
                    <!-- <input type="checkbox">-->
                   <?php 
                        echo $this->Form->input('Page.remembera', array(
                            'id' => 'remembera',
                            'type' => 'checkbox',
                            'div' => false,
                            'label' => false,
                            'checked' => empty($adm_cookie['remembera']) ? false : $adm_cookie['remembera'] == 1,
                        ));
                   
                    echo "      ". __('Remember me'); ?>
                </label>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat"> <?php echo __('Login'); ?></button>
        </div>
        <!-- /.col -->
    </div>

    <?php echo $this->Form->end(); ?>

</div><!-- /.login-box-body -->
