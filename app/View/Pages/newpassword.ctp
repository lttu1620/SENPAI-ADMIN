<div class="header"><?php echo __('New password')?></div>
<div class="body bg-gray">
    <?php
    if (isset($hasChanged)) {
        echo 'Your have changed password. Click ' . $this->Html->link('here', array(
            'controller' => 'pages',
            'action' => 'login'
        )) . ' to go home.';
    } else {
        echo $this->SimpleForm->render($renewPassword);
    }
    ?>
</div>