<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <?php
                    echo $this->SimpleForm->render($updateForm);
                ?>
                <div id="answers">
                <?php
                    echo $this->SimpleTable->render($table);
                    echo $this->Paginate->render($total, $limit);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>