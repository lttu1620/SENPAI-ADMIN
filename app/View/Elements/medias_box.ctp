<?php
/**
 * medias box
 *
 * @package View/Elements
 * @created 2015-03-31
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
?>
<div class="form-group">
    <div class="row">
        <?php foreach ($data as $d): ?>
        <div class="col-xs-6 col-md-3">
            <div class="thumbnail">
                <?php if ($d['type'] == 'image') {
                    echo '<img src="' . $d['media_url'] . '" alt="#">';
                }else{ ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <video class="embed-responsive-item" controls="">
                            <source src="<?php echo $d['media_url']; ?>">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>