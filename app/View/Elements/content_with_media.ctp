<div><?php echo nl2br($data[$key]); ?></div>
<?php
if (count($data['medias']) > 0) {
    ?>
    <div class="media-list"  id="media-<?php echo $data['id']?>">
        <?php
        foreach ($data['medias'] as $k => $item) {
            ?>
            <div class="media-item">
                <a href="#" data-toggle="modal"
                   data-image = "<?php echo $item['media_url']?>"
                   data-pos = "<?php echo $k ?>"
                   data-target="#lightBox" data-parent="#media-<?php echo $data['id']?>">
                <img src="<?php echo $this->Common->thumb($item['media_url'], '100x100') ?>">
                </a>
            </div>
        <?php
        }
        ?>
    </div>
<?php
}
?>

