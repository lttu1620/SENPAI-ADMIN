<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Senpai: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'Senpai %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>	
    <title><?php echo $meta['title'] . '｜Senpai'; if ($AppUI->is_admin == '1') echo '管理サイト'?></title>
    <meta name="description" content="<?php echo $meta['description']; ?>" />          
    <meta name="keywords" content="<?php echo $meta['keywords']; ?>" /> 
	<?php
		echo $this->Html->meta('icon');

        /*  Bootstrap 3.3.2     */
        echo $this->Html->css('/bootstrap/css/bootstrap.min.css');

        echo $this->Html->css('/bootstrap/css/bootstrap-toggle.css');

        /* FontAwesome 4.3.0 */
        echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

        /* Ionicons 2.0.0 */
        echo $this->Html->css('//code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css');

        /* Them style */
        //echo $this->Html->css('/dist/css/AdminLTE.min.css');
        echo $this->Html->css('/dist/css/AdminLTE.css');
              
        /*
         *  AdminLTE Skins. Choose a skin from the css/skins
         *   folder instead of downloading all of them to reduce the load.
         */
        echo $this->Html->css('/css/skin-application.css');

        /*  iCheck     */
        echo $this->Html->css('/plugins/iCheck/flat/blue.css');

        /*  Morris chart    */
        echo $this->Html->css('/plugins/morris/morris.css');

        /*  jvectormap  */
        echo $this->Html->css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css');

        /*  Date Picker s*/
        echo $this->Html->css('/plugins/datepicker/datepicker3.css');

        /*      Daterange picker    */
        echo $this->Html->css('/plugins/daterangepicker/daterangepicker-bs3.css');

        echo $this->Html->css('/plugins/lightbox/jquery.lightbox-0.5.css');
        
        /*      bootstrap wysihtml5 - text editor   */
        //echo $this->Html->css('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');

        foreach ($moreCss as $css) {
            echo $this->Html->css($css);
        }
        echo $this->Html->css('custom.css');
        
          /* Jqueyr 2.1.3.min */
        echo $this->Html->script('/plugins/jQuery/jQuery-2.1.3.min.js');
        
        /* Bootrap 3.3.2 */
        echo $this->Html->script('/bootstrap/js/bootstrap.min.js');
        
        /* Jquery UI 1.11.2 */
        echo $this->Html->script('//code.jquery.com/ui/1.11.2/jquery-ui.min.js');
	?>
    <script type="text/javascript">
        var baseUrl = "<?php echo $this->html->url('/'); ?>";
        var controller = "<?php echo $controller; ?>";
        var action = "<?php echo $action; ?>";
        var referer = "<?php echo $referer; ?>";        
        var url = "<?php echo $url; ?>";
        var imgBaseUrl = baseUrl+ "<?php echo Configure::read('App.imageBaseUrl'); ?>";
    </script>
</head>
<body class="skin-forapp fixed">
    <div class="wrapper">
        <?php include("header.ctp"); ?>
        <?php include("menu.ctp"); ?>
        <div class=" content-wrapper">
            <section class="content-header">
                <?php if (!empty($breadcrumb)) : ?>
                <?php echo $this->Breadcrumb->render($breadcrumb, $breadcrumbTitle); ?>
                <?php endif ?>
            </section>
            <section class="content" id="<?php echo $controller . '_' . $action; ?>">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
            </section>
        </div>
    </div>
<!--Modal-->
    <!-- Modal -->
    <div class="modal fade" id="lightBox" tabindex="-1" role="dialog" aria-labelledby="lightBoxLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" data-current="0">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary pull-left" data-event = "prev">Prev</button>
                    <span id="total-img">1/1</span>
                    <button class="btn btn-primary pull-right" data-event = "next">Next</button>
                </div>
            </div>
        </div>
    </div>
<!--End Modal-->
     <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    
    <?php
        
        /* Morris.js charts */
		echo $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js');
		echo $this->Html->script('/plugins/morris/morris.min.js');
        
        /* Sparkline   */        
		echo $this->Html->script('/plugins/sparkline/jquery.sparkline.min.js');
        
        /* jvectormap */
		echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');
		echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');
        
        /*  Query Knob Chart    */
		echo $this->Html->script('/plugins/knob/jquery.knob.js');
        
        /*  daterangepicker */
        echo $this->Html->script('/plugins/daterangepicker/daterangepicker.js');
        
         /*  datepicker */
        echo $this->Html->script('/plugins/datepicker/bootstrap-datepicker.js');
        
         /*  Bootstrap WYSIHTML5  */
        echo $this->Html->script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');
        
         /*  iCheck  */
        echo $this->Html->script('/plugins/iCheck/icheck.min.js');
        
         /*  Slimscroll  */
        echo $this->Html->script('/plugins/slimScroll/jquery.slimscroll.min.js');
        
        /*  FastClick   */
         echo $this->Html->script('/plugins/fastclick/fastclick.min.js');

        /* Admin LTE App   */
        echo $this->Html->script('/dist/js/app.min.js');
       
        echo $this->Html->script('/plugins/lightbox/jquery.lightbox-0.5.js');
        
        /* bootstrap-toggle */
        echo $this->Html->script('/bootstrap/js/bootstrap-toggle.js');
        foreach ($moreScript as $script) {
            echo $this->Html->script($script);
        }
        echo $this->Html->script('common.js');
        echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script'); 
	?> 
</body>
</html>