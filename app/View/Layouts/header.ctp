<header class="header main-header">
	<a href="<?php echo($this->Html->url('/')); ?>" class="logo">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		<?php echo $this->Html->image('logo-ascii.png'); ?>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
                <?php if ($AppUI) : ?>
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?php echo $AppUI->display_name; ?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->                        
						<li class="user-header bg-light-blue">
                            <?php if (!empty($AppUI->display_image)) : ?>
							<?php echo $this->Html->image($this->Common->thumb($AppUI->display_image), array('class' => 'img-circle'))?>                                
							<?php endif ?>
                            <p>
								<?php echo $AppUI->display_name ?>						
								<small><?php echo __('Member since') . date('Y-m', $AppUI->created) ?></small>
							</p>
						</li>
						<!-- Menu Footer-->                        
						<li class="user-footer">    
                            <?php if ($AppUI->is_admin) : ?>
                            <div class="pull-left">
								<a href="<?php echo $this->Html->Url("/admins/profile") ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i>&nbsp;<?php echo __('Profile'); ?></a>
							</div>
                            <?php endif ?>
                            <?php if (!$AppUI->is_admin) : ?>
                            <div class="pull-left">
								<a href="<?php echo $this->Html->Url("/users/profileinformation") ?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i>&nbsp;<?php echo __('Profile'); ?></a>
							</div>
                            <?php endif ?>
							<div class="pull-right">
								<a href="<?php echo $this->Html->Url("/pages/logout") ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>&nbsp;<?php echo __('Sign out'); ?></a>
							</div>                            
						</li>
					</ul>
				</li>
                <?php endif; ?>
			</ul>
		</div>
	</nav>
</header> 