<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><?php echo __("MAIN NAVIGATION");?></li>
            <li class="">
                <a href="<?php echo($this->Html->url('/')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo __('Dashboard')?></span>
                </a>
            </li>
            <!-- <li <?php if ($controller=='applications') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/applications'))?>">
                    <i class="fa fa-envelope-o"></i> <span><?php echo __('Applications Manager')?></span>
                </a>
            </li> -->
            <li <?php if ($controller=='Questions') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/questions'))?>">
                    <i class="fa fa-comment"></i> <span><?php echo __('Questions Manager')?></span>
                </a>
            </li>
            <li <?php if ($controller=='Answers') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/answers'))?>">
                    <i class="fa fa-bullhorn"></i> <span><?php echo __('Answers Manager')?></span>
                </a>
            </li>
            <li class="treeview <?php if (($controller == 'admins' && $action != 'password') || $controller=='users' || $controller=='userrecruiters' || $controller=='companies') echo "active"?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span><?php echo __('User Manager')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'admins' && $action != 'password') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/admins'))?>"><i class="fa fa-wrench"></i> <span><?php echo __('Admin Manager')?></span></a>  <li<?php if ($controller=='users') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/users'))?>"><i class="fa fa-user"></i> <span><?php echo __('Users Manager')?></span></a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php if ($controller=='settings' || ($controller == 'admins' && $action == 'password')) echo "active"?>">
                <a href="#">
                    <i class="fa fa-gear"></i> <span><?php echo __('Settings')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'global') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=global'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Global settings')?></a>
                    </li>
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'user') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=user'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default user settings')?></a>
                    </li>
                    <li<?php if ($controller == 'settings' && isset($this->request->query['type']) && $this->request->query['type'] == 'admin') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=admin'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Default admin settings')?></a>
                    </li>
                    <li<?php if ($controller == 'admins' && $action == 'password') echo " class=\"active\""?>>
                        <a href="<?php echo $this->Html->Url("/admins/password") ?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Change password'); ?></a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php if (in_array($controller, array(
                    'informations',
                    'enrollments',
                    'segments',
                    'notices',
                    'categories',
                    'usergroups')
            )) echo "active"; ?> ">
                <a href="#">
                    <i class="fa fa-gears"></i> <span><?php echo __('Master')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller=='informations') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/informations'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Informations')?></a></li>
                    <li<?php if ($controller=='enrollments') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/enrollments'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Enrollments')?></a></li>
                    <li<?php if ($controller=='segments') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/segments'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Segments')?></a></li>
                    <li<?php if ($controller=='categories') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/categories'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Categories')?></a></li>
                    <!-- <li<?php if ($controller=='notices') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/notices'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Notices')?></a></li> -->

                    <li<?php if ($controller=='usergroups') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/usergroups'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Usergroups')?></a></li>
                </ul>
            </li>
             <li class="treeview <?php if ($controller=='system') echo "active"?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span><?php echo __('System') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($action=='deletecache') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/deletecache')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Delete cache')?></a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
