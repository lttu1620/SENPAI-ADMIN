<?php

/**
 * User of model.
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class User extends AppModel
{

    public $name = 'User';
    public $table = 'users';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author thailh
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateUserInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Name must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                )
            ),
            /*'password' => array(
                'maxLength' => array(
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password must be not null or must be between 6 and 40 characters'),
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('password can not empty'),
                ),
            ),
            'password_confirm' => array(
                'maxLength' => array(
                    'rule' => array('between', 6, 40),
                    'message' => __('Your password confirm must be not null or must be between 6 and 40 characters'),
                ),
                'compare' => array(
                    'rule' => array('validate_passwords'),
                    'message' => __('The password confirm you entered do not match'),
                )
            ),*/
            'mail' => array(
                'maxLenght' => array(
                    'rule' => array('maxlength', 40),
                    'message' => __('Email must be no larger then 40 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            ),
            'user_group_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('User group can not empty'),
                )
            ),
            'enrollment_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Enrollment can not empty'),
                )
            ),
            'number' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Number can not empty'),
                )
            ),

            'grade' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Grade can not empty'),
                )
            ),
        );
        
        if (!empty($data[$this->name]['id'])) {
            $this->validate['image_url'] = array(
                'checkUploadUpdate' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUploadUpdate"),
                ),
            );
        } else {
            $this->validate['image_url'] = array(
                'checkUploadInsert' => array(
                    'allowEmpty' => false,
                    'rule' => array("checkUploadInsert"),
                    'message' => __('Image can not empty')
                ),
            );
        }

        if ($this->validates()) {
            return true;
        }
        return false;
    }


    /**
     * Validate password.
     *
     * @author thailh
     * @return bool Returns the boolean.
     */
    public function validate_passwords()
    {
        if ($this->data[$this->name]['password'] != '') {
            return $this->data[$this->name]['password'] === $this->data[$this->name]['password_confirm'];
        } else {
            return true;
        }
    }
    
    /**
	 * Verify data before upload.
	 *
	 * @author Truongnn
	 * @return bool Returns the boolean.
	 */
	public function checkUploadInsert()
	{
		$this->Common = new CommonComponent(new ComponentCollection());
		$this->Image = new ImageComponent(new ComponentCollection());
		$file = $this->Common->getFile('User.image_url');
		if (!empty($file['name'])) {
			if (!$this->Image->checkUpload($file)) {
				return $this->Common->parseArrayMessage($this->Image->errorMsg);
			}
			return true;
		}
		return false;
	}
         /**
	 * Verify data before update.
	 *
	 * @author Truongnn
	 * @return bool Returns the boolean.
	 */
	public function checkUploadUpdate()
	{
		$this->Common = new CommonComponent(new ComponentCollection());
		$this->Image = new ImageComponent(new ComponentCollection());
		$file = $this->Common->getFile('User.image_url');
		if (!empty($file['name'])) {
			if (!$this->Image->checkUpload($file)) {
				return $this->Common->parseArrayMessage($this->Image->errorMsg);
			}
		}
		return true;
	}
}
