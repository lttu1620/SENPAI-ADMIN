<?php

/**
 * Enrollment's model.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Enrollment extends AppModel {

    public $name = 'Enrollment';
    public $table = 'enrollments';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input data.
     * @return bool True if valid or otherwise.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 128),
                    'message' => __('Between 1 to 128 characters')
                ),
            ),
            'started' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Started can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                )
            ),
            'finished' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Finished can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
                'compare' => array(
                    'rule' => array('validate_dates'),
                    'message' => __('The finished must be greater or equal started'),
                )
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
    
    /**
     * Validate date.
     *
     * @author truongnn
     * @return bool True if finished >= started or otherwise.
     */
    public function validate_dates() {
        return strtotime($this->data[$this->name]['finished']) >= strtotime($this->data[$this->name]['started']);
    }
}
