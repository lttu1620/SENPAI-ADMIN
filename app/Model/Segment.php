<?php

/**
 * Segment's model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Segment extends AppModel {

    public $name = 'Segment';
    public $table = 'segments';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input data.
     * @return bool True if valid or otherwise.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 128),
                    'message' => __('Between 1 to 128 characters')
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

}
