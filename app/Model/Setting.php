<?php

/**
 * Pushmessagesendlog of model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Setting extends AppModel {

    public $name = 'Setting';
    public $table = 'settings';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 100),
                    'message' => __('Name must be no larger then 100 character long')
                )
            ),
            'description' => array(
                'maxLenght' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 255),
                    'message' => __('Description must be no larger then 255 character long')
                )
            ),
            'data_type' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Data Type can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 10),
                    'message' => __('Data Type must be no larger then 10 character long')
                )
            ),
            'type' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Type can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 10),
                    'message' => __('Type must be no larger then 10 character long')
                )
            ),
            'value' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Value can not empty'),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Verify data before the processing to insert or update image.
     *
     * @author tuancd
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateImageInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 100),
                    'message' => __('Name must be no larger then 100 character long')
                )
            ),
            'description' => array(
                'maxLenght' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 255),
                    'message' => __('Description must be no larger then 255 character long')
                )
            ),
            'data_type' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Data Type can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 10),
                    'message' => __('Data Type must be no larger then 10 character long')
                )
            ),
            'type' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Type can not empty'),
                ),
                'maxLenght' => array(
                    'rule' => array('maxlength', 10),
                    'message' => __('Type must be no larger then 10 character long')
                )
            ),
            'pattern_url_detail' => array(
                'maxLenght' => array(
                    'allowEmpty' => true,
                    'rule' => array('maxlength', 100),
                    'message' => __('Pattern Url Detail type must be no larger then 100 character long')
                )
            ),
            'value' => array(
                'checkUpload' => array(
                    'allowEmpty' => true,
                    'rule' => array("checkUploadImage_value", array()),
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }

    /**
     * Check date before upload image.
     *
     * @author tuancd
     * @return bool Returns the boolean.
     */
    public function checkUploadImage_value() {

        $this->Common = new CommonComponent(new ComponentCollection());
        $this->Image = new ImageComponent(new ComponentCollection());
        $file = $this->Common->getFile("Setting.value_image");
        if (!empty($file['name'])) {
            if (!$this->Image->checkUpload($file)) {
                return $this->Common->parseArrayMessage($this->Image->errorMsg);
            }
        }
        return true;
    }

}
