<?php

/**
 * Applications model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Application extends AppModel {

    public $name = 'Application';
    public $table = 'applications';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tuan cao
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxLength', 128),
                    'message' => __('Name must be no larger than 128 characters long')
                ),
            ),
            'url' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Url can not empty'),
                ),
                'maxLength' => array(
                    'rule' => array('maxLength', 255),
                    'message' => __('url must be no larger than 255 characters long')
                ),
                'url' => array(
                    'allowEmpty' => true,
                    'rule' => array('url', true),
                    'message' => __('Url is not available')
                ),
            ),
            'started' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Started can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                )
            ),
            'finished' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Finished can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
                'compare' => array(
                    'rule' => array('validate_dates'),
                    'message' => __('The finished must be greater or equal started'),
                )
            ),
            'priority' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Priority can not empty'),
                ),
                'Numeric' => array(
                    'rule' => 'numeric',
                    'message' => 'Input number'
                ),
            ),
            'segment_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Segment_id can not empty'),
                )
            ),
        );

        if ($this->validates())
            return true;
        return false;
    }

    /**
     * Validate date.
     *
     * @author truongnn
     * @return bool True if finished >= started or otherwise.
     */
    public function validate_dates() {
        return strtotime($this->data[$this->name]['finished']) >= strtotime($this->data[$this->name]['started']);
    }

}
