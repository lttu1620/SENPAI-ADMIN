<?php

/**
 * Categories model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Information extends AppModel {

    public $name = 'Information';
    public $table = 'Informations';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Title can not empty'),
                )
            ),
            'content' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Content can not empty'),
                )
            ),
            'started' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Started can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                )
            ),
            'finished' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Finished can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 1, 11),
                    'message' => __('Between 1 to 11 characters')
                ),
                'compare' => array(
                    'rule' => array('validate_dates'),
                    'message' => __('The finished must be greater or equal started'),
                )
            ),
            'print' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Print can not empty'),
                )
            ),
            'segment_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Segment_id can not empty'),
                )
            ),
        );

        if ($this->validates())
            return true;
        return false;
    }
    
     /**
     * Validate date.
     *
     * @author truongnn
     * @return bool True if finished >= started or otherwise.
     */
    public function validate_dates() {
        return strtotime($this->data[$this->name]['finished']) >= strtotime($this->data[$this->name]['started']);
    }
}
