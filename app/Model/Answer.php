<?php
/**
 * Answer's model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Answer extends AppModel
{
    public $name = 'Answer';
    public $table = 'answers';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Tuancd
     * @param array $data Input data.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'user_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('user_id can not empty'),
                ),
            ),
            'question_id' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Question_id can not empty'),
                ),
            ),
            'content' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Content can not empty'),
                ),
            ),
        );
        if ($this->validates()) {
            return true;
        }
        return false;
    }
}
