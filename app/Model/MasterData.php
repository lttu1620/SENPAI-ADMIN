<?php

App::uses('AppModel', 'Model');

/**
 * MasterData of model
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class MasterData extends AppModel {

    /**
     * Get all question.
     *
     * @author tuancd
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function questions_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('questions_all')->key);
        if ($result === false) {
            $result =Api::call(Configure::read('API.url_questions_all'));
            AppCache::write(Configure::read('questions_all')->key, $result, Configure::read('questions_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all categories.
     *
     * @author tuancd
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function categories_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('categories_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_categories_all'));
            AppCache::write(Configure::read('categories_all')->key, $result, Configure::read('categories_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get info global_settings.
     *
     * @author tuancd
     * @return array Returns the array.
     */
    public static function global_settings() {
        $result = AppCache::read(Configure::read('global_settings')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_settings_all'), array('type' => 'global'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'name', 'value');
            AppCache::write(Configure::read('global_settings')->key, $result, Configure::read('global_settings')->seconds);
        }
        return $result;
    }

    /**
     * Get all segment.
     *
     * @author tuancd
     * @return array Returns the array.
     */
    public static function segments_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('segments_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_segments_all'));
            AppCache::write(Configure::read('categories_all')->key, $result, Configure::read('segments_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }


    /**
     * Get all usergroups.
     *
     * @author tuancd
     * @return array Returns the array.
     */
    public static function usergroups_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('usergroups_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_usergroups_all'));
            AppCache::write(Configure::read('categories_all')->key, $result, Configure::read('usergroups_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all enrollments.
     *
     * @author tuancd
     * @return array Returns the array.
     */
    public static function enrollments_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('enrollments_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_enrollments_all'));
            AppCache::write(Configure::read('enrollments_all')->key, $result, Configure::read('enrollments_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }
    
     /**
     * Get report genaral.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function report_general() {
        $result = AppCache::read(Configure::read('report_general')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_reports_general'), array());
            AppCache::write(Configure::read('report_general')->key, $result, Configure::read('report_general')->seconds);
        }
        return $result;
    }
}
