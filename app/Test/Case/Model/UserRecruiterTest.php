<?php
App::uses('UserRecruiter', 'Model');

/**
 * UserRecruiter Test Case
 *
 */
class UserRecruiterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_recruiter'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserRecruiter = ClassRegistry::init('UserRecruiter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserRecruiter);

		parent::tearDown();
	}

}
