<?php

$modelName = $this->Admin->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Change profile');
if (empty($id)) {
    $id = $this->AppUI->id;
}
$data[$modelName]['name'] = $this->AppUI->name;

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => '/update/' . $this->request->url,
            'name' => $pageTitle
        ));
// Create Update form user
$this->UpdateForm->setModelName($modelName)
        //Create form for addupdate user
        ->setData($data)
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    if ($model->validate_profile($this->getData($modelName))) {
        $param = array(
            'id' => $id,
            'name' => $model->data[$modelName]['name']
        );
        Api::call(Configure::read('API.url_admins_addupdate'), $param);
        if (Api::getError()) {
            AppLog::info("Can not update ", __METHOD__, Api::getError());
            return $this->Common->setFlashErrorMessage(Api::getError());
        } else {
            $this->AppUI->name = $model->data[$modelName]['name'];
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        }
        $this->redirect($this->request->here(false));
    } else {
        AppLog::info("Can not update your profile", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}