<?php

$modelName = $this->Application->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Application');
$send_reservation_time = '';
if (!empty($id)) {
    $pageTitle = __('Edit Application');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_applications_detail'), $param);
   
    $this->Common->handleException(Api::getError());
}

$this->setPageTitle($pageTitle);
// Get data Segment
$lstSegment = \MasterData::segments_all();
$lstSegment = $this->Common->arrayKeyValue($lstSegment, 'id', 'name');

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/applications',
            'name' => __('Application list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// create update form
$this->UpdateForm->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('Id'),
        ))
        ->addElement(array(
            'id'        => 'name',
            'label'     => __('Name'),
             'required' => true
        ))
         ->addElement(array(
            'id'        => 'url',
            'label'     => __('Url'),
             'required' => true,
        ))
        ->addElement(array(
            'id'        => 'started',
            'type'      => 'text',
            'calendar'  => true,
            'label'     => __('Start date'),
            'required'  => true
        ))
       ->addElement(array(
            'id'        => 'finished',
            'type'      => 'text',
            'calendar'  => true,
            'label'     => __('Finish date'),
            'required'  => true
        ))
        ->addElement(array(
            'id'        => 'priority',
            'label'     => __('Priority'),
            'required'  => true
        ))
        ->addElement(array(
            'id'        => 'segment_id',
            'label'     => __('Segment'),
            'options'   => $lstSegment,
            'required'  => true
        ))
       
        ->addElement(array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type'      => 'submit',
            'value'     => __('Cancel'),
            'class'     => 'btn btn-primary pull-left',
            'onclick'   => 'return back();'
        ));
// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_applications_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}