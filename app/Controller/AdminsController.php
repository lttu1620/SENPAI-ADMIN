<?php

App::uses('AppController', 'Controller');

/**
 * AdminsController class of Admins Controller
 * 
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AdminsController extends AppController {

     /**
     * Initializes components for AdminsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Admins.
     *
     * @return void
     */
    public function index() {
        include ('Admins/index.php'); 
    }
    /**
     * Handles user interaction of view update Admins.
     *
     * @param integer $id ID value of Admin. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Admins/update.php');
    }
     
    /**
     * Handles user interaction of view password Admins.
     *
     * @param integer $id ID value of Admin. Default value is 0.
     *
     * @return void
     */
    public function password($id = 0) {
        if (empty($id)) {
            $id = $this->AppUI->id;
        }
        include ('Admins/password.php');
    }
    
    /**
     * Handles user interaction of view profile Admins.
     *
     * @param integer $id ID value of Admin. Default value is 0.
     *
     * @return void
     */
    public function profile($id = 0) {
        if (empty($id)) {
            $id = $this->AppUI->id;
        }
        include ('Admins/profile.php');
    }
}
