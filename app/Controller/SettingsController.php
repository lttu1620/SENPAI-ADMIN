<?php

/**
 * Settings Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class SettingsController extends AppController{
    public $uses = array('User', 'Setting');

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

     /**
     * index action
     * 
     * @author tuancd 
     * @return void
     */
    public function index() {
        include ('Settings/index.php'); 
    }
    
     /**
     * update action
     * 
     * @author tuancd 
     * @param int $id 
     * @return void
     */
    public function update($id=0) {
         include ('Settings/update.php');
    }
    
}
