<?php

$param = $this->data;
$param['print'] = $param['value'];

$result = Api::call("{$param['controller']}/printed", $param);

if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
exit;
