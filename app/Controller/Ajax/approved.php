<?php
$param = $this->data;
$param['question_id']= $param['value'];
$value = $param['value'];


unset($param['value']);
if($value == 1)
    $result = Api::call("{$param['controller']}/approve", $param);
else
    $result = Api::call("{$param['controller']}/reject", $param);

if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
exit;