<?php
$param = $this->data;
$apiUrl = "{$param['controller']}/disable";
$result = Api::call($apiUrl, $param);
if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
$this->Common->deleteCacheAfterDisable($param['controller']);
exit;