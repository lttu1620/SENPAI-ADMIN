
<?php


App::uses('AppController', 'Controller');

/**
 * ApplicationsController class of Applications Controller
 * 
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class ApplicationsController extends AppController {

     /**
     * Initializes components for InformationsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Applications.
     *
     * @return void
     */
    public function index() {
        include ('Applications/index.php'); 
    }
    /**
     * Handles user interaction of view update Applications.
     *
     * @param integer $id ID value of Information. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Applications/update.php');
    }
}
