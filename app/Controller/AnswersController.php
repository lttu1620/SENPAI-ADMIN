<?php

App::uses('AppController', 'Controller');

/**
 * AnswersController class of Answers Controller
 * 
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AnswersController extends AppController {

     /**
     * Initializes components for AnswersController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Admins.
     *
     * @return void
     */
    public function index() {
        include ('Answers/index.php');
    }
    /**
     * Handles user interaction of view update Answers.
     *
     * @param integer $id ID value of Admin. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
            include ('Answers/update.php');
    }
}
