   <?php

/**
 * Users Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author Tuan cao
 * @copyright Oceanize INC
 */
class UsersController extends AppController{

    /**
     * Construct
     *
     * @param null $request
     * @param null $response
     *
     * @author tuancd
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * index action
     * 
     * @author tuancd 
     * @return void
     */
    public function index() {
        include ('Users/index.php');
    }

    /**
     * update action
     *
     * @author tuancd
     * @return void
     */
    public function update($id = 0) {
        include ('Users/update.php');
    }
}
