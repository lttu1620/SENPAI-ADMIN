<?php

/**
 * Main processing for index view usergroups.
 *
 */
$modelName = $this->Usergroup->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User group list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name')
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Delete'),
            'options' => Configure::read('Config.Status'),
            'empty' => Configure::read('Config.StrAll'),
            'selected' => '0'
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'#btnSearch\').click();',
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['login_id'] = $this->getParam('login', '');
$param['disable'] = $this->getParam('disable', 0);
list($total, $data) = Api::call(Configure::read('API.url_usergroups_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'witd' => '20'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'name',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'title' => __('Name'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Delete'),
            'toggle' => true,
            'toggle-onstyle' => "danger",
            'toggle-options' => array(
                "data-on" => __("Delete"),
            ),
            'rules' => array(
                '0' => '',
                '1' => 'checked'
            ),
            'empty' => 0,
            'width' => 50,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
