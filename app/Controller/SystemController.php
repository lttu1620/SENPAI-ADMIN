<?php

App::uses('AppController', 'Controller');

/**
 * System Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class SystemController extends AppController
{

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * deletecache action
     * 
     * @author thailh
     * @return void 
     */
    public function deletecache() {
        $files = array();
        $files = array_merge($files, glob(CACHE . '*')); // remove cached data
        $files = array_merge($files, glob(CACHE . 'css' . DS . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'js' . DS . '*'));  // remove cached js           
        $files = array_merge($files, glob(CACHE . 'models' . DS . '*'));  // remove cached models           
        $files = array_merge($files, glob(CACHE . 'persistent' . DS . '*'));  // remove cached persistent           
        foreach ($files as $f) {
            if (is_file($f)) {
                unlink($f);
            }
        }  
        Router::url(Configure::read('FE.Host'). "system/deletecache", true );
        @file_get_contents(Configure::read('FE.Host'). "system/deletecache");
        $this->Common->setFlashSuccessMessage(__('Cache deleted successfully'));
        return $this->redirect($this->request->referer());
    }
    
}
