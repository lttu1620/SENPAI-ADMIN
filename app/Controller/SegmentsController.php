<?php

App::uses('AppController', 'Controller');

/**
 * SegmentsController class of Segments Controller
 * 
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class SegmentsController extends AppController {

     /**
     * Initializes components for SegmentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Segments.
     *
     * @return void
     */
    public function index() {
        include ('Segments/index.php'); 
    }
    /**
     * Handles user interaction of view update Segments.
     *
     * @param integer $id ID value of Segment. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Segments/update.php');
    }
}
