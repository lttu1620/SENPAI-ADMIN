<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
// Check remember userName and password
$this->Cookie->httpOnly = true;
if (!$this->Auth->loggedIn()) {
    if ($this->Cookie->read('remember_admin_cookie')) {
        $adm_cookie = $this->Cookie->read('remember_admin_cookie');
    } 
}

//Set value
$this->set(compact('adm_cookie'));

if ($this->request->is('post')) {
    if ($model->validateLogin($this->getData($modelName))) {
        $param['password'] = $model->data[$modelName]['password'];
        if (!empty($model->data[$modelName]['login'])) {
            $this->dispatchEvent('Admin.beforeLogin', $param);
            $param['login_id'] = $model->data[$modelName]['login'];
            if ($this->startAdminLogin($param)) {
                $this->dispatchEvent('Admin.afterLogin', $param);
                // did they select the remember me checkbox?
                if ($model->data[$modelName]['remembera'] == 1) {
                    $model->data[$modelName]['admin_password'] = $model->data[$modelName]['password'];
                    unset($model->data[$modelName]['password']);
                    // write the cookie
                    $this->Cookie->write('remember_admin_cookie', $model->data[$modelName], true, '2 weeks');
                }
                //$this->Cookie->write('last_login_url', $isAdminUrl, true, '2 weeks');
                return $this->redirect($this->Auth->redirect());
            }
        } 
    }
    AppLog::info("Login fail", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage(__('Invalid Login ID or password. Please try again'));
}