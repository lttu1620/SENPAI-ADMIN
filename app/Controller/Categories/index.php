<?php

$modelName = $this->Category->name;

// process disable / enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Category list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Delete'),
            'options' => Configure::read('Config.Status'),
            'empty' => Configure::read('Config.StrAll'),
            'selected' => '0'
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'id-asc' => __('ID Asc'),
                'id-desc' => __('ID Desc'),
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['disable'] = $this->getParam('disable', 0);
list($total, $data) = Api::call(Configure::read('API.url_categories_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'width' => '60',
            'style' => 'background:{color}'
        ))
        ->addcolumn(array(
            'id' => 'id',
            'title' => __('ID'),
            'value' => '{id}',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'name',
            'title' => __('Name'),
            'value' => '{name}',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}'
        ))
        ->addColumn(array(
            'id' => 'color',
            'title' => __('Color'),
            'value' => '{color}',
        ))
        ->addColumn(array(
            'id' => 'sort',
            'title' => __('Sort'),
            'value' => '{sort}',
        ))
        ->addColumn(array(
            'type' => 'link',
            'th_title' => __('Followers'),
            'title' => __('View'),
            'href' => '/' . $this->controller . '/follower/{id}',
            'width' => '100',
            'button' => true,
        ))
        ->addColumn(array(
            'type' => 'link',
            'th_title' => __('Subcategories'),
            'title' => __('View'),
            'href' => '/subcategories?category_id={id}',
            'width' => '120',
            'button' => true,
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Delete'),
            'toggle' => true,
            'toggle-onstyle' => "danger",
            'toggle-options' => array(
                "data-on" => __("Delete"),
            ),
            'rules' => array(
                '0' => '',
                '1' => 'checked'
            ),
            'empty' => 0,
            'width' => 50,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
