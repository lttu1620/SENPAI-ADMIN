<?php

$modelName = $this->Enrollment->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add enrollment');
if (!empty($id)) {
    $pageTitle = __('Edit enrollment');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_enrollments_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
        ->add(array(
            'link' => $this->request->base . '/enrollments',
            'name' => __('Enrollment list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form 
$this->UpdateForm
        ->setModelName($modelName)
        ->setData($data)
        ->addElement(array(
            'id' => 'id',
            'type' => 'hidden',
            'label' => __('id'),
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name'),
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'id' => 'started',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Started'),
        ))
        ->addElement(array(
            'id' => 'finished',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Finished'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

// process when submit form
if ($this->request->is('post')) {    
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_enrollments_addupdate'), $model->data[$modelName]);        
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}