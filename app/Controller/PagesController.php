<?php

/**
 * PagesController class of Pages Controller
 *
 * @package	Controller  
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
App::uses('AppController', 'Controller');

/**
 * Initializes components for PagesController class.
 */
class PagesController extends AppController {

    /**
     * Index Action
     *
     * @return void
     */
    public function index() {
        include ('Pages/index.php');
    }

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function display() {

        include ('Pages/index.php');

        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Action login   
     *  
     * @author thailh 
     * @return void
     */
    public function login() {
        include ('Pages/login.php');
    }

    /**
     * Action register   
     *  
     * @author thailh 
     * @return void
     */
    public function register() {
        include ('Pages/register.php');
    }


    /**
     * Action logout   
     *  
     * @author thailh 
     * @return void
     */
    public function logout() {
        include ('Pages/logout.php');
    }

    /**
     * Action forgetpassword   
     *  
     * @author thailh 
     * @return void
     */
    public function forgetpassword() {
        include ('Pages/forget-password.php');
    }

    /**
     * Action newpassword   
     *  
     * @author thailh 
     */
    public function newpassword($token = null) {
        include ('Pages/new-password.php');
    }

    /**
     * Action sendmailsuccess   
     *  
     * @author thailh 
     * @return void
     */
    public function sendmailsuccess() {
        // include ('Pages/new-password.php');
    }

    /**
     * Action sendmailnotification   
     *  
     * @author thailh 
     */
    public function sendmailnotification() {
        // do not delete
    }

    /**
     * Action privacypolicy   
     *  
     * @author thailh 
     * @return void
     */
    public function privacypolicy() {
        include ('Pages/privacypolicy.php');
    }
    
    public function language() {
        $l = $this->getParam('l', 'eng');
        if ($l != 'jpn') $l = 'eng'; 
        $this->Session->write('language', $l);        
        $this->redirect('/');
    }    

}
