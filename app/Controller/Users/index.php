<?php

/**
 * Created by PhpStorm.
 * User: caodinhtuan
 * Date: Apr/1/15
 * Time: 08:59
 */
$modelName = $this->User->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('User list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

$lstuserGroup = $this->Common->arrayKeyValue(\MasterData::usergroups_all(), 'id', 'name');
$lstenrollment = $this->Common->arrayKeyValue(\MasterData::enrollments_all(), 'id', 'name');

// Create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'number',
            'label' => __('Number')
        ))
        ->addElement(array(
            'id' => 'name',
            'label' => __('Name')
        ))
        ->addElement(array(
            'id' => 'grade',
            'label' => __('Grade')
        ))
        ->addElement(array(
            'id' => 'user_group_id',
            'label' => __('User group'),
            'options' => $lstuserGroup,
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'enrollment_id',
            'label' => __('Enrollment'),
            'options' => $lstenrollment,
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'sex_id',
            'label' => __('Gender'),
            'options' => Configure::read('Config.searchGender'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Delete'),
            'options' => Configure::read('Config.Status'),
            'empty' => Configure::read('Config.StrAll'),
            'selected' => '0'
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'name-asc' => __('Name Asc'),
                'name-desc' => __('Name Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
                'updated-asc' => __('Updated Asc'),
                'updated-desc' => __('Updated Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'#btnSearch\').click();',
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['login_id'] = $this->getParam('login', '');
$param['disable'] = $this->getParam('disable', 0); 
list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
        ->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'witd' => '20'
        ))
        ->addColumn(array(
            'id' => 'image_url',
            'type' => 'image',
            'title' => __('Image'),
            'src' => '{image_url}',
            'image_type' => 'user',
            'width' => '60'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'number',
            'type' => 'link',
            'title' => __('Number'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'type' => 'link',
            'href' => '/' . $this->controller . '/update/{id}',
            'title' => __('User name'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'user_group_name',
            'title' => __('User group'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'enrollment_name',
            'title' => __('Enrollment'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'grade',
            'title' => __('Grade'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'width' => 120
        ))
        /* ->addColumn(array(
          'type' => 'link',
          'th_title' => __('Password'),
          'title' => __('Change'),
          'href' => '/' . $this->controller . '/password/{id}',
          'button' => true,
          'width' => 100,
          )) */
        ->addColumn(array(
            'type' => 'link',
            'title' => __('Setting'),
            'href' => '/usersettings/index/{id}',
            'width' => 80,
            'button' => true
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Delete'),
            'toggle' => true,
            'toggle-onstyle' => "danger",
            'toggle-options' => array(
                "data-on" => __("Delete"),
            ),
            'rules' => array(
                '0' => '',
                '1' => 'checked'
            ),
            'empty' => 0,
            'width' => 50,
        ))
        ->setDataset($data)
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
