<?php
/**
 * Created by PhpStorm.
 * User: caodinhtuan
 * Date: Apr/1/15
 * Time: 08:59
 */

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (empty($id)) {
   $this->Common->handleException(null);
}
$pageTitle = __('Edit user');
$param['id'] = $id;
$data[$modelName] = Api::call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());


$lstuserGroup = $this->Common->arrayKeyValue(\MasterData::usergroups_all(), 'id', 'name');
$lstenrollment = $this->Common->arrayKeyValue(\MasterData::enrollments_all(), 'id', 'name');


$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/users',
        'name' => __('User list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// Create Update form
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id' => 'number',
        'type' => 'input',
        'readonly' => true,
        'label' => __('Number'),
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'autocomplete' => 'off',
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'sex_id',
        'label' => __('Gender'),
        'required' => true,
        'options' => Configure::read('Config.searchGender'),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'mail',
        'readonly' => true,
        'required' => true,
        'label' => __('Email')
    ))
    ->addElement(array(
        'id' => 'grade',
        'label' => __('Grade'),
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'user_group_id',
        'label' => __('User group'),
        'options' => $lstuserGroup,
        'empty' => Configure::read('Config.StrChooseOne'),
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'enrollment_id',
        'label' => __('Enrollment'),
        'options' => $lstenrollment,
        'empty' => Configure::read('Config.StrChooseOne'),
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'image_url',
        'type' => 'file',
        'image' => true,
        'label' => __('Image'),
        'class' => 'resize_button_upload',
        'required' => true,
    ))
    ->addElement(array(
        'id' => 'memo',
        'type' => 'textarea',
        'rows' => '10',
        'label' => __('Memo'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

// process when submit form
if ($this->request->is('post')) {
   
    if ($model->validateUserInsertUpdate($this->getData($modelName))) { 
        // Processing upload Image
        if (!empty($_FILES['data']['name'][$modelName]['image_url'])) {
            $model->data[$modelName]['image_url'] = $this->Image->uploadImage(
                    "{$modelName}.image_url", 'users'
            );
        } elseif (isset($model->data[$modelName]['image_url']['remove'])) {
            $model->data[$modelName]['image_url'] = '';
        } else {
            unset($model->data[$modelName]['image_url']);
        }
        $id = Api::call(Configure::read('API.url_users_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}