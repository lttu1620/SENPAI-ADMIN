<?php

App::uses('AppController', 'Controller');

/**
 * UsergroupsController class of Usergroups Controller
 * 
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UsergroupsController extends AppController {

     /**
     * Initializes components for UsergroupsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Usergroups.
     *
     * @return void
     */
    public function index() {
        include ('Usergroups/index.php'); 
    }
    /**
     * Handles user interaction of view update Usergroups.
     *
     * @param integer $id ID value of Usergroup. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Usergroups/update.php');
    }
}
