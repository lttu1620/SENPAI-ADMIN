<?php

App::uses('AppController', 'Controller');

/**
 * EnrollmentsController class of Enrollments Controller
 * 
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class EnrollmentsController extends AppController {

     /**
     * Initializes components for EnrollmentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Enrollments.
     *
     * @return void
     */
    public function index() {
        include ('Enrollments/index.php'); 
    }
    /**
     * Handles user interaction of view update Enrollments.
     *
     * @param integer $id ID value of Enrollment. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Enrollments/update.php');
    }
}
