<?php

App::uses('AppController', 'Controller');

/**
 * QuestionsController class of Questions Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class QuestionsController extends AppController
{

    /**
     * Initializes components for QuestionsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Questions.
     *
     * @return void
     */
    public function index()
    {
        include('Questions/index.php');
    }

    /**
     * Handles user interaction of view update Questions.
     *
     * @param integer $id ID value of Question. Default value is 0.
     *
     * @return void
     */
    public function update($id = 0)
    {   
        if(empty($id)){
            return $this->redirect("/{$this->controller}");
        }
        include('Questions/update.php');
    }


    /**
     * Handles user interaction of view favorite Questions.
     *
     * @param integer $question_id ID value of Question. Default value is 0.
     *
     * @return void
     */
    public function favorite($question_id = 0)
    {
        include('Questions/favorite.php');
    }

    /**
     * Handles user interaction of view view Questions.
     *
     * @param integer $question_id ID value of Question. Default value is 0.
     *
     * @return void
     */
    public function view($question_id = 0)
    {
        if ($question_id != 0) {
            include('Questions/view.php');
        }
    }

    /**
     * Handles user interaction of view like Questions.
     *
     * @param integer $question_id ID value of Question. Default value is 0.
     *
     * @return void
     */
    public function like($question_id = 0)
    {
        if ($question_id != 0) {
            include('Questions/like.php');
        }
    }
}
