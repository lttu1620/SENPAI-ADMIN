<?php

/**
 * IndexsController class of Indexs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class LoginlogsController extends AppController {

    /**
     * Initializes components for LoginlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Loginlogs.
     * 
     * @return void
     */
    public function index() {
        include ('Loginlogs/index.php');
    }

}
