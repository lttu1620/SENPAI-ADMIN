<?php

/**
 * Main processing for index view informations.
 *
 */
$modelName = $this->Information->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Information list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// Create search form 
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'title',
            'label' => __('Title')
        ))
        ->addElement(array(
            'id' => 'content',
            'label' => __('Content')
        ))
        ->addElement(array(
            'id' => 'started',
            'label' => __('Start date'),
            'type' => 'text',
            'calendar' => true,
            'empty' => '',
        ))
        ->addElement(array(
            'id' => 'finished',
            'label' => __('Finish date'),
            'type' => 'text',
            'calendar' => true,
            'empty' => '',
        ))
        ->addElement(array(
            'id' => 'print',
            'label' => __('Print'),
            'options' => Configure::read('Config.searchStatus'),
            'empty' => Configure::read('Config.StrAll'),
        ))
        ->addElement(array(
            'id' => 'disable',
            'label' => __('Delete'),
            'options' => Configure::read('Config.Status'),
            'empty' => Configure::read('Config.StrAll'),
            'selected' => '0'
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'title-asc' => __('Title Asc'),
                'title-desc' => __('Title Desc'),
                'started-asc' => __('Started Asc'),
                'started-desc' => __('Started Desc'),
                'finished-asc' => __('Finished Asc'),
                'finished-desc' => __('Finished Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
                'updated-asc' => __('Updated Asc'),
                'updated-desc' => __('Updated Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
            'onchange' => 'javascript: $(\'#btnSearch\').click();',
        ))
        ->addElement(array(
            'type' => 'submit',
            'id' => 'btnSearch',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right'
        ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['login_id'] = $this->getParam('login', '');
$param['disable'] = $this->getParam('disable', 0);
list($total, $data) = Api::call(Configure::read('API.url_informations_list'), $param, false, array(0, array())); //d($data, 1);
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
        ->addColumn(array(
            'id' => 'item',
            'name' => 'items[]',
            'type' => 'checkbox',
            'value' => '{id}',
            'width' => '20'
        ))
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'title',
            'type' => 'link',
            'title' => __('Title'),
            'href' => '/' . $this->controller . '/update/{id}',
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'content',
            'title' => __('Content'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'title' => __('Created'),
            'type' => 'date',
            'width' => 120,
            'hide' => true
        ))
        ->addColumn(array(
            'id' => 'print',
            'type' => 'checkbox',
            'title' => __('Print'),
            'toggle' => true,
            'toggle-onstyle' => "success",
            'toggle-options' => array(
                "data-on" => __("Print"),
            ),
            'class' => 'printed',
            'rules' => array(
                '0' => '',
                '1' => 'checked',
            ),
            'empty' => '0',
            'width' => 50,
        ))
        ->addColumn(array(
            'id' => 'started',
            'title' => __('Started'),
            'type' => 'date',
            'width' => 120,
        ))
        ->addColumn(array(
            'id' => 'finished',
            'title' => __('Finished'),
            'type' => 'date',
            'width' => 120,
        ))
        ->addColumn(array(
            'id' => 'disable',
            'type' => 'checkbox',
            'title' => __('Delete'),
            'toggle' => true,
            'toggle-onstyle' => "danger",
            'toggle-options' => array(
                "data-on" => __("Delete"),
            ),
            'rules' => array(
                '0' => '',
                '1' => 'checked'
            ),
            'empty' => 0,
            'width' => 50,
        ))
        ->setDataset($data)
        ->setMergeColumn(array(
            'title' => array(
                array(
                    'field' => 'created',
                    'before' => __('created') . " : ",
                    'after' => ''
                ),
            )
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Add new'),
            'class' => 'btn btn-primary btn-addnew',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Disable'),
            'class' => 'btn btn-primary btn-disable',
        ))
        ->addButton(array(
            'type' => 'submit',
            'value' => __('Enable'),
            'class' => 'btn btn-primary btn-enable',
        ));
