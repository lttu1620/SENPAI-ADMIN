<?php
/**
 * User: caodinhtuan
 * Date: Mar/17/15
 * Time: 09:18
 */

$modelName = $this->Question->name;

// Process disable/enable
$this->doGeneralAction($modelName);
$param['report'] = $this->getParam('report', 0);
$listCategory = MasterData::categories_all(array('id', 'name'));

// create breadcrumb
$pageTitle = __('Question list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
//d($param['report'], 1);
// Create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'category_id',
        'label' => __('Category name'),
        'options' => $listCategory,
        'empty' => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id' => 'content',
        'label' => __('Content')
    ))
   ->addElement(array(
        'id' => 'status',
        'label' => __('Approve'),
        'options' => Configure::read('Config.StatusApprove'),
        'empty' => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id' => 'disable',
        'label' => __('Delete'),
        'options' => Configure::read('Config.Status'),
        'empty' => Configure::read('Config.StrAll'),
        'selected' => '0'
    ))
    ->addElement(array(
        'id' => 'sort',
        'label' => __('Sort'),
        'options' => array(
            'nice_count-asc' => __('Like count Asc'),
            'nice_count-desc' => __('Like count Desc'),

            'answer_count-asc' => __('Answer count Asc'),
            'answer_count-desc' => __('Answer count Desc'),

            'favorite_count-asc' => __('Favorite count Asc'),
            'favorite_count-desc' => __('Favorite count Desc'),

            'created-asc' => __('Created Asc'),
            'created-desc' => __('Created Desc'),

            'updated-asc' => __('Updated Asc'),
            'updated-desc' => __('Updated Desc'),
        ),
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'limit',
        'label' => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
        'onchange' => 'javascript: $(\'#btnSearch\').click();',
    ))
    ->addElement(array(
        'id' => 'report',
        'type' => 'checkbox',
        'checked' => empty($param['report']) ? false : true,
        'after' => "<label for='report' > <span class='nomarlText'> &nbsp;" . __('Report violation') . "</span></lable>",
        'label' => false,
        'hiddenField'=>false,
        'class' => '',
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['disable'] = $this->getParam('disable', 0);
list($total, $data) = Api::call(Configure::read('API.url_questions_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id' => 'id',
        'type' => 'link',
        'title' => __('ID'),
        'href' => '/' . $this->controller . '/update/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id' => 'user',
        'th_title' => __('user'),
        'width' => '100',
    ))
    ->addColumn(array(
        'id' => 'user_image_url',
        'type' => 'image',
        'src' => '{user_image_url}',
        'image_type' => 'user',
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'user_name',
        'type' => 'link',
        'href' => '/users/update/{user_id}',
        'empty' => '',
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'content',
        'title' => __('Content'),
        'empty' => '',
         'width' => '300',
        'type' =>'has_media',
        'media_options' => array(
            'element' => 'content_with_media'
        )
    ))
    ->addColumn(array(
        'id' => 'flag',
        'th_title' => __('Publication Flag'),
        'width' => '80',
    ))
    ->addColumn(array(
        'id' => 'to_univ',
        'type' => 'icon',
        'title' => __('University'),
        'class' =>'disabled',
        'icon_prefix' => 'to_univ-',
        'icon_valiations' => array(
            '0' => 'off',
            '1' => 'on',
        ),
        'empty' => '0',
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'to_high',
        'type' => 'icon',
        'title' => __('Highscool'),
        'icon_prefix' => "to_high-",
        'icon_valiations' => array(
            '0' => 'off',
            '1' => 'on',
        ),
        'empty' => '0',
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'to_teacher',
        'type' => 'icon',
        'title' => __('Teacher'),
        'icon_prefix' => "to_teacher-",
        'icon_valiations' => array(
            '0' => 'off',
            '1' => 'on',
        ),
        'empty' => '0',
        'hidden' => true
    ))      
    ->addColumn(array(
        'id' => 'answer_count',
        'type' => 'link',
        'title' => __('Answer count'),
        'href' => '/' . $this->controller . '/update/{id}#answers',
        'width' => '80',
        'empty' => 0,
    ))
    ->addColumn(array(
        'id' => 'favorite_count',
        'type' => 'link',
        'title' => __('Favorite count'),
        'href' => '/' . $this->controller . '/favorite/{id}',
        'width' => '80',
         'empty' => 0,
    ))
     ->addColumn(array(
        'id' => 'nice_count',
        'type' => 'link',
        'title' => __('Like count'),
        'href' => '/' . $this->controller . '/like/{id}',
        'width' => '80',
         'empty' => 0,
    ))
    ->addColumn(array(
        'id' => 'status',
        'type' => 'checkbox',
        'title' => __('Approve'),
        'toggle' => true,
        'toggle-onstyle' => "success",
        'toggle-options' => array(
            "data-on" => __("Publication"),
            "data-offstyle" => "warning"
        ),
        'class' =>'approved',
        'rules' => array(
            '0' => '',
            '1' => 'checked',
            '2' => ''
        ),
        'empty' => '0',
        'width' => 50,
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Delete'),
        'toggle' => true,
        'toggle-onstyle' => "danger",
        'toggle-options' => array(
            "data-on" => __("Delete"),
        ),
        'rules' => array(
            '0' => '',
            '1' => 'checked'
        ),
        'empty' => 0,
        'width' => 50,
    ))
    ->addColumn(array(
        'id' => 'edit_link',
        'type' => 'link',
        'th_title' => __('Edit'),
        'title' => __('Edit'),
        'href' => '/' . $this->controller . '/update/{id}',
        'button' => true,
        'width' => '50'
    )) 
    ->setDataset($data)
    ->setMergeColumn(array(
        'user' => array(
            array(
                'field' => 'user_name',
                'before' => '',
                'after' => '<br />',
                'class'=>'user_name'
            ),
            array(
                'field' => 'user_image_url',
                'before' => '',
                'after' => '',
                'class'=>'user_image_url'
            ),
        ),
        'flag' => array(
            array(
                'field' => 'to_univ',
                'before' => '',
                'after' => '<br />',
                'class'=>'to_univ'
            ),
            array(
                'field' => 'to_high',
                'before' => '',
                'after' => '<br />',
                'class'=>'to_high'
            ),
            array(
                'field' => 'to_teacher',
                'before' => '',
                'after' => '',
                'class'=>'to_teacher'
            ),            
        ),       
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('update same time:').__('Approve'),
        'class' => 'btn btn-primary btn-approve',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('update same time:').__('Reject'),
        'class' => 'btn btn-primary btn-reject',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('update same time:').__('Delete'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('update same time:').__('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
