<?php
/**
 * User: tuancd
 * Date: Mar/20/15
 * Time: 10:00
 */
$modelName = $this->Question->name;

// Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Question like list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/questions',
        'name' => __('Question list')
    ))
    ->add(array(
        'name' => $pageTitle,
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['question_id'] = $question_id;
list($total, $data) = Api::call(Configure::read('API.url_questionnices_list'), $param, false, array(0, array()));


$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
    ->addColumn(array(
        'id' => 'item',
        'name' => 'items[]',
        'type' => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))    
    ->addColumn(array(
        'id' => 'user_name',
        'type' => 'link',
        'title' => __('User name'),
        'width' => '200',
        'empty' => ''
    ))
    ->addColumn(array(
        'id' => 'category_name',
        'title' => __('Category name'),
        'empty' => '',
        'width' => '300',
    ))
   
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 100,
    ))
    ->setDataset($data)
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type' => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
