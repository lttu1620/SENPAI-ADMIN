<?php
/**
 * User: caodinhtuan
 * Date: Mar/17/15
 * Time: 09:54
 */
$modelName = $this->Question->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Question');
if (!empty($id)) {
    $pageTitle = __('Edit Question');
    $param = $this->getParams(array(
        'id' => $id, 
        'answer' => 1,
        'page' => 1, 
        'limit' => Configure::read('Config.pageSize'))
    );
    $data[$modelName] = Api::call(Configure::read('API.url_questions_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/questions',
        'name' => __('Question list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// Create list catergory
$listCategory = MasterData::categories_all();
$listCategory = $this->Common->arrayKeyValue($listCategory, 'id', 'name');
// process when submit form
if ($this->request->is('post')) {    
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($id)) {
            $result = Api::call(Configure::read('API.url_questions_update'), $model->data[$modelName]);
        } else {
            $result = Api::call(Configure::read('API.url_questions_add'), $model->data[$modelName]);
        }
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}

// Create Update form
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'user_id',
        'type' => 'input',
        'readonly' => !empty($id),
        'required' => true,
        'label' => __('User ID')
    ))
    ->addElement(array(
        'id' => 'category_id',
        'type' => 'input',
        'required' => true,
        'options' => $listCategory,
        'label' => __('Category')
    ))
    ->addElement(array(
        'id' => 'content',
        'type' => 'textarea',
        'label' => __('Content'),        
        'required' => true
    ))
    ->addElement(array(
        'id' => 'to_univ',
        'label' => __('To_univ'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ))
    ->addElement(array(
        'id' => 'to_high',
        'label' => __('To_high'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ))
    ->addElement(array(
        'id' => 'to_teacher',
        'label' => __('To_teacher'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ))
    ->addElement(array(
        'type' => 'element',
        'name' => 'medias_box',
        'data' => $data[$modelName]['medias']
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

list($total, $answers) = $data[$modelName]['answers'];
$this->set('total', $total);
$this->set('limit', $param['limit']);

if (!empty($answers)) {
    $this->SimpleTable
        ->addColumn(array(
            'id' => 'id',
            'type' => 'link',
            'title' => __('ID'),
            'href' => '/answers/update/{id}',
            'width' => '30'
        ))
        ->addColumn(array(
            'id' => 'user_name',
            'type' => 'link',
            'title' => __('User name'),
            'href' => '/users/update/{user_id}',
            'width' => '200',
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'content',
            'title' => __('Content'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'nice_count',
            'title' => __('Likes count'),
            'empty' => '0',
            'width' => 100,
        ))
        ->setDataset($answers);
}