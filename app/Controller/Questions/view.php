<?php
/**
 * Created by PhpStorm.
 * User: caodinhtuan
 * Date: Mar/17/15
 * Time: 11:04
 */

$modelName = $this->Question->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Question');
if (!empty($question_id)) {
    $pageTitle = __('Edit Question');
    $param['id'] = $question_id;
    $data[$modelName] = Api::call(Configure::read('API.url_questions_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/questions',
        'name' => __('Question list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// Create list catergory
$listCategory = MasterData::categories_all(array('id', 'name'));
// Create Update form
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id' => 'user_id',
        'type' => 'input',
        'required' => true,
        'label' => __('User ID')
    ))
    ->addElement(array(
        'id' => 'category_id',
        'type' => 'input',
        'required' => true,
        'options' => $listCategory,
        'label' => __('Category')
    ))
    ->addElement(array(
        'id' => 'content',
        'label' => __('Content'),
        'required' => true
    ))
    ->addElement(array(
        'id' => 'to_univ',
        'label' => __('To_univ'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ))
    ->addElement(array(
        'id' => 'to_high',
        'label' => __('To_high'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ))
    ->addElement(array(
        'id' => 'to_teacher',
        'label' => __('To_teacher'),
        'required' => true,
        'options' => Configure::read('Config.Status'),
    ));

    if ($this->AppUI->is_admin) {
        $this->UpdateForm
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ));
    }

    $this->UpdateForm
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($id)) {
            $id = Api::call(Configure::read('API.url_answers_update'), $model->data[$modelName]);
        }
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}