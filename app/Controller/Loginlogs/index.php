<?php

$modelName = $this->Loginlog->name;
// create breadcrumb
$pageTitle = __('Login log list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'nickname',
            'label' => __('Nickname')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'user_name-asc' => __('Name Asc'),
                'user_name-desc' => __('Name Desc'),
                'user_nickname-asc' => __('Nickname Asc'),
                'user_nickname-desc' => __('Nickname Desc'),
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'created-asc' => __('Login time Asc'),
                'created-desc' => __('Login time Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_loginlogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'user_name',
            'title' => __('Username'),
        ))
        ->addColumn(array(
            'id' => 'user_nickname',
            'title' => __('Nickname'),
            'width' => '350'
        ))
        ->addColumn(array(
            'id' => 'user_email',
            'title' => __('Email'),
            'width' => '350'
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Login time'),
            'width' => '120'
        ))
        ->setDataset($data);
