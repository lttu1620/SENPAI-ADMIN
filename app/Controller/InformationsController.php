
<?php


App::uses('AppController', 'Controller');

/**
 * InformationsController class of Informations Controller
 * 
 * @package Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class InformationsController extends AppController {

     /**
     * Initializes components for InformationsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Informations.
     *
     * @return void
     */
    public function index() {
        include ('Informations/index.php'); 
    }
    /**
     * Handles user interaction of view update Informations.
     *
     * @param integer $id ID value of Information. Default value is 0.
     *
     * @return void
     */
    public function update($id=0) {
         include ('Informations/update.php');
    }
}
