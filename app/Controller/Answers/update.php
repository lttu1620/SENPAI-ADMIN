<?php
/**
 * User: tuancd
 * Date: Mar/16/15
 * Time: 16:54
 */

$modelName = $this->Answer->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Answer');
if (!empty($id)) {
    $pageTitle = __('Edit Answer');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_answers_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/answers',
        'name' => __('Answers list')
    ))
    ->add(array(
        'name' => $pageTitle
    ));

// Create list question 


// Create Update form
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('id'),
    )) 
    ->addElement(array(
        'id' => 'question_content',
        'type'=>'textarea',
        'label' => __('Question content'),
        'readonly' => true
    ))
    ->addElement(array(
        'id' => 'user_id',
        'type' => 'input',
        'readonly' => !empty($id),
        'required' => true,
        'label' => __('User ID')
    ))
    ->addElement(array(
        'id' => 'content',
        'type'=>'textarea',
        'label' => __('Content'),
        'required' => true
    ))
    ->addElement(array(
        'type' => 'element',
        'name' => 'medias_box',
        'data' => $data[$modelName]['medias']
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        if (!empty($id)) {
            $result = Api::call(Configure::read('API.url_answers_update'), $model->data[$modelName]);
        } else {
            $result = Api::call(Configure::read('API.url_answers_add'), $model->data[$modelName]);
        }
        if (!empty($result) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}