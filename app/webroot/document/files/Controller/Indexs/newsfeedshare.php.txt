<?php

$this->AppHtml->css('morris/morris.css');
$this->AppHtml->script('plugins/morris/morris.min.js');
$this->AppHtml->script('plugins/hightchart/highcharts.js');

$pageTitle = __('Newsfeed share report');
$modelName = 'Newsfeedshare';
//Create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));
$param = $this->getParams(array(
    'type' => 'line_chart',
    'mode' => 'day',
    ));

if (!isset($param['date_from'])) {
    $param['date_from'] = date('Y-m-d', strtotime('last month'));
    $this->setParam('date_from', $param['date_from']);
}
if (!isset($param['date_to'])) {
    $param['date_to'] = date('Y-m-d');
    $this->setParam('date_to', $param['date_to']);
}
// Create search form 
$this->SearchForm
    ->setModelName($modelName)
    ->setAttribute('type', 'get')
     ->addElement(array(
        'id' => 'date_from',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date from'),
        'value' => $param['date_from']
    ))
    ->addElement(array(
        'id' => 'date_to',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date to'),
        'value' => $param['date_to']
    ))
    ->addElement(array(
        'id' => 'type',
        'label' => __('Chart type'),
        'options' => Configure::read('Config.searchChartType'),
    ))
    ->addElement(array(
        'id' => 'mode',
        'label' => __('Chart mode'),
        'options' => Configure::read('Config.searchChartMode'),
    ))
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$result = Api::call(Configure::read('API.url_reports_newsfeed_share'), $param);
$count_field = array('facebook_count', 'twitter_count', 'google_count');
switch ($param['mode']) {
    case 'week':
        $result = $this->Common->weekChartData(array(
            'data' => $result,
            'date_field' => 'date_report',
            'count_field' => $count_field,
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;
    case 'month' :
        $result = $this->Common->monthChartData(array(
            'data' => $result,
            'date_field' => 'date_report',
            'count_field' => $count_field,
            'date_from' => $param['date_from'],
            'date_to' => $param['date_to'],
        ));
        break;
    default :
        foreach ($result as &$item) {
            $item['labels'] = $item['date_report'];
        }
}

// Custom data to show 
$_categories = array();
$index = 0;
foreach ($count_field as $key) {
    $_data[$index] = array();
    foreach ($result as $item) {
        if ($index == 0)
            $_categories[] = $item['labels'];
        $_data[$index]['name'] = $key;
        $_data[$index]['data'][] = intval($item[$key]);
    }
    $index ++;
}
if (Api::getError()) {
    $this->set('data', '\'\'');
} else {
    $this->set('categories', '\'' . json_encode($_categories) . '\'');
//    $this->set('data', !empty($result) ? json_encode($result) : '\'\'');
    $this->set('data', !empty($result) ? json_encode($_data) : '\'\'');
    $this->set('xkey', '\'date_report\'');
    $this->set('ykeys', '\'' . json_encode(array('facebook_count', 'twitter_count', 'google_count')) . '\'');
    $this->set('labels', '\'' . json_encode(array('Facebook', 'Twitter', 'Google+')) . '\'');
    $this->set('type', '\'' . $param['type'] . '\'');
    $this->set('viewmode', '\'' . $param['mode'] . '\'');
}
