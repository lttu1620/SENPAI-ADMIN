<?php
AppLog::info("Resend register email", __METHOD__); 
$param = $this->data;
Api::call(Configure::read('API.url_users_resendregisteremail'), $param);
if (Api::getError()) {
    AppLog::info("Can not resent email", __METHOD__, $param);
    echo __('System error, please try again');    
} else {
    echo __('Email has been resent successfully');
}
exit;
