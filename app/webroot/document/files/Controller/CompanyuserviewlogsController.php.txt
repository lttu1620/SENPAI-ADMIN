<?php

/**
 * CompanyuserviewlogsController class of Companyuserviewlogs Controller
 *
 * @package	Controller
 * @copyright Oceanize INC
 */
class CompanyuserviewlogsController extends AppController {

    /**
     * Initializes components for CompanyuserviewlogsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

     /**
     * Handles user interaction of view index Companyuserviewlogs.
     * 
     * @return void
     */
    public function index() {
        include ('Companyuserviewlogs/index.php');
    }

}

