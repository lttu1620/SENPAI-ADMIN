<?php

App::uses('AppController', 'Controller');

/**
 * DepartmentsController class of Departments Controller
 *
 * @package	Controller
 * @copyright Oceanize INC
 */
class DepartmentsController extends AppController {

    /**
     * Initializes components for DepartmentsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }
    
    /**
     * Handles user interaction of view index Departments.
     * 
     * @return void
     */
    public function index() {
        include ('Departments/index.php');
    }

   /**
     * Handles user interaction of view update Departments.
     * 
     * @param integer $id ID value of Departments. Default value is 0.
     * 
     * @return void
     */
    public function update($id = 0) {
        include ('Departments/update.php');
    }
    
}
