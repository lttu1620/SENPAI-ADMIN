<?php

/**
 * Contacts of model
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Contact extends AppModel {

    public $name = 'Contact';
    public $table = 'contacts';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 40),
                    'message' => __('Name must be between 2 to 40 characters')
                ),
            ),
            'email' => array(
                'maxLenght' => array(
                    'allowEmpty' => false,
                    'rule' => array('maxlength', 255),
                    'message' => __('Email must be no larger then 255 character long')
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Email can not empty'),
                ),
                'rule3' => array(
                    'rule' => '/^[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)+([A-Za-z0-9]{2,4}|museum)$/',
                    'message' => __('Please supply a valid email address'))
            ),
            'subject' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Subject can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 200),
                    'message' => __('subject must be between 2 to 200 characters')
                )
            ),
            'content' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Content can not empty')
                )
            ),
            'website' => array(
                'url' => array(
                    'allowEmpty' => true,
                    'rule' => array('url', true),
                    'message' => __('Url is not available')
                ),
            ),
        );
        return $this->validates();
    }

}

